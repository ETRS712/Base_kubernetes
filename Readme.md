# Première approche Kubernetes -- utilisation kubectl 

## installation environnement d'apprentissage Kubernetes

### Minikube

Téléchargement (et installation de minikube) suivre le mode d'emploi décrit sur le site de [Minikube](https://minikube.sigs.k8s.io/docs/start/).

Démarrage/Création d'un cluster

```bash
minikube start
```

Obtention du statut du cluster

```bash
minikube status
```

Arrêt du cluster

```bash
minikube stop
```
Suppression du cluster

```bash
minikube delete
```

### Microk8s

Si vous avez installé snapd (disponible par défaut sur dans Ubuntu, à installer avec Debian ou CentOS), 
l'installation de [MicroK8s](https://microk8s.io/) est facile :

```bash
sudo snap install microk8s --classic
```

Ensuite il faut ajouter de l'utilisateur courant dans le group microk8s (afin de pouvoir l'utiliser sans être admin), 
puis se reconnecter dans un nouveaux shell

```bash
sudo usermod -a -G microk8s $(whoami)
```

Obtention du statut du cluster

```bash
microk8s status --wait-ready
```

Démarrage d'un cluster

```bash
microk8s start
```

Arrêt du cluster

```bash
microk8s stop
```

Désinstallation complète

```bash
sudo snap remove --purge microk8s
```

## Démarrage avec Kubectl

La commande kubectl est la commande de base pour interagir avec les clusters K8s.  
Si vous avez installé [Docker-Desktop](https://www.docker.com/products/docker-desktop) (Windows / MacOS), 
vous devriez avoir une version de kubectl installée avec Docker. 
Sinon il est possible de l'installer séparément en suivant le [mode d'emploi suivant](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/).

Si vous ne voulez pas installer kubectl, Minikube et Microk8s fournissent une commande équivalente : 
- microk8s : `microk8s.kubectl`
- minikube : `minikube kubectl --`

### Configuration de kubectl (pas nécessaire si vous utilisez `microk8s.kubectl` ou `minikube kubectl --` )

Kubectl peut être utilisé pour gérer plusieurs clusters Kubernetes. 
Les configurations (clusters, contexts, users) sont, par défaut recherchées dans le fichier `.kube/config` 
(dans le répertoire de l'utilisateur), mais peuvent être définies soit avec l'option  `--kubeconfig`, 
soit en utilisant la variable d'environnement `KUBECONFIG`.

Minikube, lors de la création/destruction d'un cluster, met de lui même à jour le fichier `.kube/config`.  
Avec Microk8s on peut récupérer la configuration en utilisant la commande `microk8s.config`.

Affichage de la liste des contextes kubernetes configurés :

```bash
kubectl config get-contexts
```
On obtient alors la liste des contextes configurés :

```bash
CURRENT   NAME             CLUSTER                 AUTHINFO         NAMESPACE
          docker-desktop   docker-desktop          docker-desktop
          kind-kind        kind-kind               kind-kind        default
          microk8s         microk8s-cluster-ipv6   admin
*         microk8s-ipv4    microk8s-cluster-ipv4   admin
``` 

#### Ajout du contexte de microk8s et choix du contexte courant

On peut par exemple écraser fichier de configuration par défaut : `microk8s.config > ~/.kube/config`  
On peut aussi mettre la nouvelle configuration dans un nouveau fichier et utiliser ensuite, 
soit l'option `--kubeconfig`, soit la variable d'environnement `KUBECONFIG` : 

```bash
microk8s.config > ~/.kube/config.microk8s
```
ce qui permet par la suite de l'utiliser comme ci-dessous :

```bash
kubectl --kubeconfig=~/.kube/config.microk8s config view
```
ou encore comme cela :

```bash
export KUBECONFIG=~/.kube/config:~/.kube/config.microk8s
kubectl config view
```

Choix du contexte courant : commande `kubectl config use-context <contexte>`  
Exemple : 

```bash
kubectl config use-context docker-desktop
```

Vous pouvez vérifier que vous pouvez vous connecter à votre cluster par exemple en tapant la commande suivante : 
`kubectl cluster-info`


#### Configuration de la complétion

Si vous avez installé kubectl avec votre gestionnaire de paquets préféré (yum, apt, etc..), il existe souvent un paquet 
qui permet d'activer la complétion pour les commandes kubectl.  
Sinon vous pouvez l'activer directement avec kubectl, en utilisant la commande suivante (pour un shell bash) : 
`source <(kubectl completion bash)`

### Commandes kubectl de base

#### Affichage des informations sur les ressources du cluster kubernetes

Informations sur les pods de namespace par défaut :  `kubectl get pods` (ou `microk8s.kubectl get pods` ou `minikube kubectl -- get pods`).

Informations sur les pods de tous les namespaces (option `-A` ou `--all-namespaces`) avec un affichage plus complet (option `-o wide` ou `--outpout=wide`) : 

```bash
kubectl get pods --output wide -A

NAMESPACE     NAME                                         READY   STATUS    RESTARTS   AGE     IP             NODE   NOMINATED NODE   READINESS GATES
kube-system   coredns-86f78bb79c-qrwpk                     1/1     Running   1          2d5h    10.1.251.195   vm41   <none>           <none>
kube-system   metrics-server-8bbfb4bdb-zv68z               1/1     Running   0          2d5h    10.1.251.196   vm41   <none>           <none>
kube-system   kubernetes-dashboard-7ffd448895-6sj6r        1/1     Running   0          2d5h    10.1.251.197   vm41   <none>           <none>
kube-system   dashboard-metrics-scraper-6c4568dc68-x4gkq   1/1     Running   0          2d5h    10.1.251.198   vm41   <none>           <none>
kube-system   hostpath-provisioner-5c65fbdb4f-gmzfz        1/1     Running   0          2d5h    10.1.251.199   vm41   <none>           <none>
kube-system   calico-node-tlqmz                            1/1     Running   1          2d5h    10.20.0.241    vm41   <none>           <none>
kube-system   calico-kube-controllers-847c8c99d-bfmn5      1/1     Running   0          2d5h    10.1.251.194   vm41   <none>           <none>
default       nginx                                        1/1     Running   0          170m    10.1.251.227   vm41   <none>           <none>
```

Obtention d'une ressource (dans l'exemple le _pod_ de nom "nginx") sous forme yaml/json : `kubectl get pod/nginx --output=yaml`

Liste des services d'un namespace : `kubectl get services --namespace=kube-system`

Liste des déploiements du namespace par défaut ayant un label "app" avec la valeur "nginx" : `kubectl get deployments -l app=nginx`

#### Démarrage d'un pod

On peut créer et démarrer un pod avec la commande `kubectl run` :

Exemple création d'un pod (de nom "nginx") avec un container nginx dans le namespace par défaut : 

```bash
kubectl run nginx --image=nginx:1.17
```

Démarrage d'un pod en précisant une commande à exécuter pour démarrer le pod : 

```bash
kubectl run utils --image=debian:10 -- bash -c "while [ 1 ] ; do sleep 5 ; done"
```

En utilisant l'option `--dry-run=client`, on peur tester que la commande est bonne avant de l'exécuter pour de vrai :  
`kubectl run utils --image=debian:10 --dry-run=client -- bash -c "while [ 1 ] ; do sleep 5 ; done"` .

Si on rajoute l'option `--output=yaml` (ou `--output=json`), on obtient également sous forme yaml (ou json) 
la ressource kubernetes qui est créée ou modifiée.

```bash
kubectl run utils --image=debian:10 --dry-run=client --output=yaml -- bash -c "while [ 1 ] ; do sleep 5 ; done" 

apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: utils
  name: utils
spec:
  containers:
  - args:
    - bash
    - -c
    - while [ 1 ] ; do sleep 5 ; done
    image: debian:10
    name: utils
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

Il est alors possible de sauvegarder cette sortie yaml ([pod.yaml](pod.yaml)) pour la modifier et l'appliquer plus tard.

```bash
kubectl run utils --image=debian:10 --dry-run=client --output=yaml -- bash -c "while [ 1 ] ; do sleep 5 ; done" > pod.yaml
kubectl apply -f pod.yaml
```

Il est aussi possible de modifier une ressource qui a déjà été déployée en utilisant la commande `kubectl edit`. 

Exemples : 

```bash
kubectl edit pod/nginx
kubectl edit -f pod.yaml
```

#### Création / modification d'un déploiement

La plupart du temps les pods, dans le cadre du déploiement d'applications, ne seront pas crées 
avec la commande `run` mais plutôt à l'aide soit d'objets kubernetes de type `Deployment` ou `StatefulSet`. 

La création d'un déploiement peut se faire soit à partir de la description yaml du déploiement, soit en utilisant 
la commande `kubectl create`. 
Par exemple la création d'un déploiement de 4 pods nginx pourrait se faire en utilisant la commande suivante : 

```bash
kubectl create deployment nginx-dep --image=nginx:1.16 --replicas=4
```
ou si on a une version yaml du deployement [deploy-nginx.yaml](deploy-nginx.yaml)

```bash
kubectl create -f deploy-nginx.yaml
```

Il est alors possible de modifier le déploiement, une fois celui-ci créé, 
soit modifiant directement la ressource déployée avec la commande `kubectl edit`, 
soit en en modifiant le fichier yaml initial, puis en utilisant la commande `kubectl apply` sur le fichier yaml modifié. 

Edition de la ressource en ligne : 

```bash
kubectl edit deployment/nginx-dep 
```

Application du fichier de déploiement yaml modifié : 

```bash
kubectl apply -f deploy-nginx.yaml 
```

Il est également possible de modifier certaines caractéristiques spécifiques en utilisant d'autres commandes kubectl.  
Par exemple on peut changer le nombre de pods du déploiement : `kubectl scale deployment/nginx-dep --replicas=10`  
ou mettre à jour l'image utilisée pour le déploiement : `kubectl set image deployment/nginx-dep nginx=nginx:1.18`

#### Création / modification d'un service

Même si chaque pod a une adresse IP (interne au cluster), il n'est pas possible de choisir cette adresse. 
En outre il existe de multiples raisons qui peuvent amener le cluster à supprimer et recréer des pods et 
donc à changer leurs adresses IP. Les pods et leurs adresses IP doivent donc être vus comme des objets éphémères.

L'accès vers les applications se fait donc plutôt en utilisant des services kubernetes auxquels est associés, 
de façon dynamique, un groupe de pods. Pour résumer les services kubernetes fournissent une interface stable 
vers des services fournis par des pods dont le nombre, les adresses IP, la localisation et l'existence même 
peuvent être amenés à varier fortement au cours du temps.

La création d'un service peut se faire avec la commande `kubectl create` ou la commande `kubectl expose`.

Exemples de création d'un service pour le déploiement précédent.
avec `kubectl expose` :

```bash
kubectl expose deployment/nginx-dep --port=80
```

avec `kubectl create` et un fichier yaml, [service.yaml](service.yaml) : 

```bash
kubectl create -f service.yaml
```

Un service aura sa propre adresse IP, attribuée par le cluster kubernetes 
(il est possible de la choisir en utilisant l'option `--clusterip=<adddres-ip>`), 
et permettra d'associer des ports sur service kubernetes avec des ports des pods associés au service.

Par exemple si, dans le déploiement, l'image utilisée crée des conteneurs utilisant le port 8080, 
alors on peut avoir un [service kubernetes](service_hello.yaml) où on utilisera à la place le port 80.

```bash
kubectl create deployment hello --replicas=4 --image=gcr.io/google-samples/kubernetes-bootcamp:v1
kubectl expose deployment/hello --port=80 --target-port=8080
```

Kubernetes permet de créer 4 types de services (ClusterIP, NodePort, LoadBalancer, External). 

Par défaut c'est le type `ClusterIP` qui est utilisé. Il permet de d'attribuer aux services une adresse IP 
interne au cluster, il s'agit donc de services internes au cluster. 

Les services de type `NodePort` permettent en plus d'associer aux ports du service des ports sur 
les nœuds du cluster. Ces services sont donc accessibles de l'extérieur du cluster 
(en utilisant les adresses externes des nœuds du cluster). 

Exemple : 

```bash
kubectl expose deployment/hello --name=hello-nodeport --port=80 --target-port=8080 --type=NodePort
``` 

Dans la [description du service](service_hello_nodeport.yaml), le champ `.spec.ports[*].nodePort` 
permet de choisir le port externe utilisé pour la publication. 
S'il n'est pas défini le cluster choisira alors un port libre à l'intérieur d'une plage donnée.

Le troisième type de services correspond au type `LoadBalancer`. 

Il permet de publier le service vers l'extérieur du cluster en utilisant 
une adresse IP externe au cluster (attribuée par le _load-balancer_). 
Habituellement le _load-balancer_ est un service externe au cluster, 
fourni et configuré par l'hébergeur du cluster kubernetes (Google, Amazon, Microsoft, OVH, Scaleway, ...) 
en fonction des plages d'adresses IP qui ont été achetées et associées au cluster. 

Dans le cas d'un cluster _bare-metal_, il est éventuellement possible d'utiliser [MetalLB](https://metallb.universe.tf/) 
pour configurer les plages d'adresses IP sur lesquelles les services kubernetes seront publiés. 

Exemple :

```bash
kubectl expose deployment/hello --name=hello-nodeport --port=80 --target-port=8080 --type=LoadBalancer
``` 

Le paramètre `--load-balancer-ip=<ext-ip>`, s'il est utilisé peut permettre de demander une adresse IP spécifique, 
sinon le _load-balancer_ assignera une adresse IP choisie en fonction de sa configuration. 

Dans la [description du service](service_hello_ip_externe.yaml) le champ `.spec.loadBalancerIP` 
permet parfois (cela dépend du _load-balancer_ utilisé) de choisir l'adresse IP utilisée pour la publication du service. 
S'il n'est pas défini le _load-balancer_ attribuera au service une adresse IP éphémère.

Le quatrième et dernier type de service `ExternalName`, correspond lui à des services externes 
au cluster kubernetes. Il s'agit en pratique de faire correspondre à un service externe au cluster 
un service kubernetes qui pourra, à l'intérieur du cluster, être utilisé comme s'il s'agissait d'un service 
fournit par le cluster. 

Exemple : 

```bash
kubectl create service externalname my-service --external-name=my.external.service.com --dry-run=client -o yaml
``` 

Le paramètre `--external-name=<dns-name>`, permet de préciser le nom DNS du service externe au cluster. 

Dans la [description du service](service_externalname.yaml), 
celui-ci est défini au moyen du champ `.spec.ExternalName`.

#### Utilisation d'un proxy entre le serveur d'API du cluster et l'hôte local 

La commande `kubectl proxy` permet d'établir un proxy entre l'hôte local (accès via localhost) et le serveur d'API du cluster kubernetes. 
Par défaut le port 8001 sera utilisé pour accéder localement au serveur d'API (http://localhost:8001). 

On peut utiliser ce proxy pour récupérer les descriptions (json) des ressources kubernetes déployées sur le cluster.  
Par exemple l'url http://localhost:8001/api/v1/namespaces/default/pods/nginx permet d'obtenir la description, 
sous la forme d'un objet json, du pod nginx déployé dans le namespace _default_. 

Il est aussi possible de faire des requêtes sur les pods et les services publiées sur le cluster en utilisant des urls de la forme suivante :
- `http://localhost:8001/api/v1/namespaces/<namespace>/pods/[protocol:]<pod>[:port]/proxy` : requête sur un pod
- `http://localhost:8001/api/v1/namespaces/<namespace>/services/[protocol:]<service>[:port]/proxy` : requête sur un service

Par exemple la requête suivante http://localhost:8001/api/v1/namespaces/default/services/http:hello:80/proxy  permettra d'envoyer 
une requête sur le service _hello_ dans le namespace _default_ en utilisant le protocol _http_ et le port _80_.
```bash
curl  -L http://localhost:8001/api/v1/namespaces/default/services/http:hello:80/proxy 

Hello Kubernetes bootcamp! | Running on: hello-7494fd677b-bp7rr | v=1
```

### Utilisation du dashboard 

Fréquemment dans les clusters kubernetes est aussi déployé un 
[dashboard](https://kubernetes.io/fr/docs/tasks/access-application-cluster/web-ui-dashboard/) 
qui fournit, sous la forme d'un site web, une interface de gestion du cluster. 

Généralement il n'est pas possible d'y accéder depuis l'extérieur du cluster (pour des raisons de sécurité), 
il faut alors utiliser le proxy kubectl pour y accéder. 

#### Installation et utilisation sur minikube

Minikube permet d'installer le dashboard comme un addon : 
on peut l'installer avec la commande `minikube addons enable dashboard`.
Il suffit alors de lancer dans un terminal la commande `kubectl proxy` ou 
`minikube kubectl -- proxy` pour pouvoir y accéder en utilisant l'url suivante (utilisation du proxy kubectl) : 
http://127.0.0.1:8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/

Minikube permet également d'utiliser la commande `minikube dashboard` pour installer, si besoin, 
le dashboard, puis démarrer le proxy et ouvrir le dashboard dans une des fenêtres de votre navigateur. 

#### Installation et utilisation sur microk8s

Microk8s fournit aussi le dashboard sous forme d'un addon activable avec la commande `microk8s enable dashboard`.  
Il suffit alors de lancer dans un terminal la commande `kubectl proxy` ou 
`microk8s.kubectl -- proxy` pour pouvoir y accéder en utilisant l'url suivante (utilisation du proxy kubectl) : 
http://127.0.0.1:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

Pour plus de sécurité, il vous sera demandé de vous authentifier avec un token (un jeton). 
Vous pouvez en récupérer un avec la commande `kubectl -n kube-system describe secret default` ou 
`microk8s.kubectl -n kube-system describe secret default` (recopiez la valeur du champ token dans le navigateur).

Il est aussi possible de créer un nouvel utilisateur disposant de privilèges d'administration et 
de créer un jeton pour cet utilisateur 
(cf. [tuto ici](https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md)).

#### Installation du dashboard à partir du dépôt git du projet

Toutes les distributions Kubernetes n'incluent pas le dashboard (comme un addon ou une sous autre forme). 
Il reste néanmoins généralement possible d'installer le dashboard comme une application kubernetes 
en utilisant le [dépôt git du projet](https://github.com/kubernetes/dashboard). 
C'est le cas par exemple du cluster kubernetes installé avec Docker Desktop.

Une fois le dashboard installé à partir du dépôt git avec la commande suivante : 

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended.yaml
```

l'accès au dashboard se fait comme précédemment : 
- lancement du proxy kubernetes : `kubectl proxy`
- ouverture dans un navigateur html de la page correspondant au service déployé (utilisation du proxy kubectl) : 
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/
- récupération d'un token de connexion à recopier dans le formulaire d'authentification : 
`kubectl -n kube-system describe secret default` 

Comme précédemment, il est aussi possible de créer un nouvel utilisateur avec des droits administrateur et 
de créer un token pour cet utilisateur.

## Quelques liens utiles

- Documentation Minikube : https://minikube.sigs.k8s.io/docs/
- Documentation Microk8s : https://microk8s.io/docs
- Documentation kubernetes : https://kubernetes.io/docs/home/
- Tutoriels kubernetes : 
    - Minikube : https://kubernetes.io/docs/tutorials/hello-minikube/
    - bases de kubernetes (tutoriel interactif) : https://kubernetes.io/docs/tutorials/kubernetes-basics/
    - kubectl pour les utilisateur de docker : https://kubernetes.io/docs/reference/kubectl/docker-cli-to-kubectl/
- Référence : 
    - kubectl : https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands
    - kubernetes API : https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/



 





